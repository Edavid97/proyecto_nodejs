var trabajo = require('../../models/trabajos');
var mongoose = require('mongoose');

describe('testing trabajos', function() {
    beforeEach(function (done) {
        var mongoDB = 'mongodb+srv://admin_OrgLab:97122110420@cluster0-xifxk.mongodb.net/organizador?retryWrites=true&w=majority';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true}, function(err, res){
            if(err){
                console.log('Error conectando a Atlas: '+ err );
            }else{
                console.log('Conectado a Atlas');
                done();
            }
        });
    });

    afterEach(function (done) {
        trabajo.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe('trabajo.createInstance', () => {
        it('crea una instancia de trabajo', () => {
            var trabajos = trabajo.createInstance(1, 'madera', 200000, [4.744697, -74.122623]);
            expect(trabajos.id).toBe(1);
            expect(trabajos.tipo).toBe('madera');
            expect(trabajos.valor).toBe(200000);
            expect(trabajos.ubicacion[0]).toEqual(4.744697);
            expect(trabajos.ubicacion[1]).toEqual(-74.122623);
        });
    });

    describe('trabajo.allTrabajos', () => {
        it('comienza vacia', (done) => {
            trabajo.allTrabajos(function(err, trabajos){
                expect(trabajos.length).toBe(0);
                done();
            });
        });
    });

    describe('trabajo.add', () => {
        it('agrega solo un trabajo', (done) => {
            var aTrabajo = new trabajo({id: 1, tipo: 'madera', valor: 200000});
            trabajo.add(aTrabajo, function(err, newTrabajo){
                if (err) console.log(err);
                trabajo.allTrabajos(function(err, trabajos){
                    expect(trabajos.length).toEqual(1);
                    expect(trabajos[0].id).toEqual(aTrabajo.id);

                    done();
                });
            });
        });
    });

    describe('trabajo.findById', () => {
        it('devuelve el trabajo con id 1', (done) => {
            trabajo.allTrabajos(function(err, trabajos){
                expect(trabajos.length).toBe(0);

                var aTrabajo = new trabajo({id: 1, tipo: 'madera', valor: 200000});
                trabajo.add(aTrabajo, function(err, newTrabajo){
                    if (err) console.log(err);
                    var aTrabajo2 = new trabajo({id: 2, tipo: 'construccion', valor: 550000});
                    trabajo.add(aTrabajo2, function(err, newTrabajo){
                        if (err) console.log(err);
                        trabajo.findById(1, function(err, targetTrabajo){
                            expect(targetTrabajo.id).toBe(aTrabajo.id);
                            expect(targetTrabajo.tipo).toBe(aTrabajo.tipo);
                            expect(targetTrabajo.valor).toBe(aTrabajo.valor);

                            done();
                        });
                    });
                });
            });
        });
    });

    describe('trabajo.removeById', () => {
        it('remover el trabajo con id 2', (done) => {
            trabajo.allTrabajos(function(err, trabajos){
                expect(trabajos.length).toBe(0);
                var aTrabajo = new trabajo({id: 1, tipo: 'madera', valor: 200000});
                trabajo.add(aTrabajo, function(err, newTrabajo){
                    if (err) console.log(err);
                    var aTrabajo2 = new trabajo({id: 2, tipo: 'construccion', valor: 550000});
                    trabajo.add(aTrabajo2, function(err, newTrabajo){
                        if (err) console.log(err);
                        trabajo.removeById(2, function(err, targetTrabajo){
                            trabajo.allTrabajos(function(err, trabajos){
                                expect(trabajos.length).toBe(1);
                                expect(trabajos[0].id).toBe(1);
                                done();
                            });
                        });
                    });
                });
            });
        });
    });
});