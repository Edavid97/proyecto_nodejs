var trabajo = require('../../models/trabajos');
var server = require('../../bin/www')
var request = require('request');
var mongoose = require('mongoose');

var base_url = 'http://localhost:3000/api/trabajos';

describe('testing trabajos API', function() {
    beforeEach(function (done) {
        var mongoDB = 'mongodb+srv://admin_OrgLab:97122110420@cluster0-xifxk.mongodb.net/organizador?retryWrites=true&w=majority';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true}, function(err, res){
            if(err){
                console.log('Error conectando a Atlas: '+ err );
            }else{
                console.log('Conectado a Atlas');
                done();
            }
        });
    });

    afterEach(function (done) {
        trabajo.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe('GET Trabajos /', () => {
        it('status 200', (done) => {
            var aTrabajo = new trabajo({id: 1, tipo: 'madera', valor: 200000});
            trabajo.add(aTrabajo, function(err, newTrabajo){
                if (err) console.log(err);
                var aTrabajo2 = new trabajo({id: 2, tipo: 'madecor', valor: 350000});
                trabajo.add(aTrabajo2, function(err, newTrabajo){
                    if (err) console.log(err);
                    request.get(base_url, function(error, response, body){
                        var result = JSON.parse(body).trabajos;
                        expect(response.statusCode).toBe(200);
                        expect(result.length).toBe(2);
        
                        done();
                    });
                });
            });
        });
    });

    describe('POST trabajos /create', () => {
        it('status 200', (done) => {
            var headers = {'content-Type': 'application/json'}; 
            var aTrabajo = '{"id": 11, "tipo": "altillo", "valor": 350000, "lat":  4.744207, "lng": -74.119863}'
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aTrabajo
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var Trabajo = JSON.parse(body).trabajo;
                expect(Trabajo.id).toBe(11);
                expect(Trabajo.tipo).toBe("altillo");
                done();
            });
        });
    });

    describe('DELETE trabajos /delete', () => {
        it('status 204', (done) => {
            var aTrabajo = new trabajo({id: 1, tipo: 'madera', valor: 200000});
            trabajo.add(aTrabajo, function(err, newTrabajo){
                if (err) console.log(err);
                trabajo.allTrabajos(function(err, trabajos){
                    if (err) console.log(err);
                    expect(trabajos.length).toBe(1);
                    var headers = {'content-Type' : 'application/json'};
                    var aID = '{"id": 1}'
                    request.delete({
                        headers: headers,
                        url: base_url + '/delete',
                        body: aID
                    }, function(error, response, body){
                        expect(response.statusCode).toBe(204);
                        trabajo.allTrabajos(function(err, trabajos2){
                            if (err) console.log(err);
                            expect(trabajos2.length).toBe(0);
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('UPDATE trabajos /update/:id', () => {
        it('status 200', (done) => {
            var aTrabajo = new trabajo({id: 1, tipo: 'madera', valor: 200000, ubicacion: [4.744697, -74.122623]});
            trabajo.add(aTrabajo, function(err, newTrabajo){
                if (err) console.log(err);
                trabajo.allTrabajos(function(err, trabajos){
                    if (err) console.log(err);
                    expect(trabajos.length).toBe(1);
                    var headers = {'content-Type' : 'application/json'};
                    var aTrabajo = '{"id": 1,"tipo": "madera","valor": 123123,"lat": 4.744207,"lng": -74.119863}';
                    request.post({
                        headers: headers,
                        url: base_url + '/update/1',
                        body: aTrabajo
                    }, function(error, response, body){
                        expect(response.statusCode).toBe(200);
                        trabajo.findById(1, function(err, targetTrabajo){
                            if (err) console.log(err);
                            expect(targetTrabajo.valor).toBe(123123);
                            done();
                        });
                    });
                });
            });
        });
    });
});