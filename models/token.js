var mongoose = require('mongoose');

const Schema = mongoose.Schema;

const tokenSchema = new Schema ({
    _userID: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'usuarios'},
    token: { type: String, required: true},
    createdAt: { type: Date, required: true, default: Date.now, expires: 43200}
});

module.exports = mongoose.model('token', tokenSchema);