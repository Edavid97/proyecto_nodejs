var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var trabajoSchema = new Schema({
    id: Number,
    tipo: String,
    valor: Number,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true}
    }
})

trabajoSchema.methods.toString = function() {
    return 'id: '+this.id + ' | tipo: '+this.tipo + " | valor: "+this.valor;
};

trabajoSchema.statics.allTrabajos = function(cb) {
    return this.find({}, cb);
};

trabajoSchema.statics.createInstance = function(id, tipo, valor, ubicacion){
    return new this({
        id: id,
        tipo: tipo,
        valor: valor,
        ubicacion: ubicacion
    });
};

trabajoSchema.statics.add = function(aTrabajo, cb){
    this.create(aTrabajo, cb);
};

trabajoSchema.statics.findById = function(aID, cb){
    return this.findOne({id: aID}, cb);
};

trabajoSchema.statics.removeById = function(aID, cb){
    return this.deleteOne({id: aID}, cb);
};

module.exports = mongoose.model('trabajo', trabajoSchema);