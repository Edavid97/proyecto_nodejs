var express = require('express');
var router = express.Router();
var usuariosController = require('../../controllers/api/usuariosControllerAPI');

router.get('/', usuariosController.usuario_list);
router.post('/create', usuariosController.usuario_create);

module.exports = router;