var express = require('express');
var router = express.Router();
var trabajosController = require('../../controllers/api/trabajoControllerAPI');

router.get('/', trabajosController.trabajo_list);
router.post('/create', trabajosController.trabajo_create);
router.delete('/delete', trabajosController.trabajo_delete);
router.post('/update/:id', trabajosController.trabajo_update);

module.exports = router;