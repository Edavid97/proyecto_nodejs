var express = require('express');
var router = express.Router();
var trabajosController = require('../controllers/trabajos');

router.get('/', trabajosController.trabajo_list);
router.get('/create', trabajosController.trabajo_create_get);
router.post('/create', trabajosController.trabajo_create_post);
router.post('/delete/:id', trabajosController.trabajo_delete_post);
router.get('/update/:id', trabajosController.trabajo_update_get);
router.post('/update/:id', trabajosController.trabajo_update_post);

module.exports = router;