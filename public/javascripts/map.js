var map = L.map('main_map').setView([4.744697, -74.122623], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([-34.6012424,-58.3861497]).addTo(map)

$.ajax({
    dataType: "json",
    url: "api/trabajos",
    success: function(result){
        console.log(result);
        result.trabajos.forEach(function(Trabajo) {
            L.marker(Trabajo.ubicacion, {title: Trabajo.id}).addTo(map)
        });
    }
})

