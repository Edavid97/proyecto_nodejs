var trabajo = require('../models/trabajos');
var mongoose = require('mongoose');

exports.trabajo_list = function(req, res){
    trabajo.allTrabajos(function(err, trabajos){
        res.render('trabajos/index', {trabajosLista: trabajos})
    });
}

exports.trabajo_create_get = function(req, res){
    res.render('trabajos/create');
}

exports.trabajo_create_post = function(req, res){
    var Trabajo = trabajo.createInstance(req.body.id, req.body.tipo, req.body.valor);
    Trabajo.ubicacion = [req.body.lat, req.body.lng];
    trabajo.add(Trabajo);
    
    res.redirect('/trabajos');
}

exports.trabajo_update_get = function(req, res){
    trabajo.findById(req.params.id, function(err, Trabajo){
        res.render('trabajos/update', {trabajo: Trabajo});
    });
}

exports.trabajo_update_post = function(req, res){

    trabajo.findOneAndUpdate({"id": req.params.id},{"id": req.body.id, "tipo": req.body.tipo, "valor": req.body.valor, "ubicacion": [req.body.lat, req.body.lng]}, function(){
        res.redirect('/trabajos');
    });
}

exports.trabajo_delete_post = function(req, res){
    trabajo.removeById(req.body.id, function(){
        res.redirect('/trabajos');
    });
}