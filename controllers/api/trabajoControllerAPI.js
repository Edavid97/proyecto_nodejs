var trabajo = require('../../models/trabajos');

exports.trabajo_list = function(req, res){
    trabajo.allTrabajos(function(err, trabajos){
        res.status(200).json({
            trabajos: trabajos
        });
    });
}

exports.trabajo_create = function(req, res){
    var Trabajo = trabajo.createInstance(req.body.id, req.body.tipo, req.body.valor);
    Trabajo.ubicacion = [req.body.lat, req.body.lng];
    trabajo.add(Trabajo);

    res.status(200).json({
        trabajo: Trabajo
    });
}

exports.trabajo_delete = function(req, res){
    trabajo.removeById(req.body.id, function(){
        res.status(204).send();
    });
}

exports.trabajo_update = function(req, res){
    trabajo.findOneAndUpdate({"id": req.params.id},{"id": req.body.id, "tipo": req.body.tipo, "valor": req.body.valor, "ubicacion": [req.body.lat, req.body.lng]}, function(err, trabajoUpdate){
        res.status(200).json({
            trabajo: trabajoUpdate
        });
    });
}