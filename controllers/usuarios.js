var usuario = require('../models/usuarios');
var mongoose = require('mongoose');

exports.usuario_list = function(req, res){
    usuario.find({}, (err, usuarios) => {
        res.render('usuarios/index', {usuarios: usuarios})
    });
}

exports.usuario_create_get = function(err, res){
    res.render('usuarios/create', {errors: {}, usuario: new usuario()});
}

exports.usuario_create_post = function(req, res){
    if(req.body.password != req.body.confirm_password){
        res.render('usuarios/create', {errors: {confirm_password: {message: 'No coincide con el password ingresado'}}, usuario: new usuario({nombre: req.body.nombre, email: req.body.email})});
        return;
    }

    usuario.create({nombre: req.body.nombre, email: req.body.email, password: req.body.password}, function(err, newUsuario) {
        if(err){
            res.render('usuarios/create', {errors: err.errors, usuario: new usuario({nombre: req.body.nombre, email: req.body.email})});
        }else{
            newUsuario.enviar_email_bienvenida();
            res.redirect('/usuarios')
        }
    });
}

exports.usuario_update_get = function(req, res){
    usuario.findById(req.params.id, function(err, Usuario){
        if(Usuario){
            console.log(Usuario);
        }else{
            console.log('Error');
        }
        res.render('usuarios/update', {errors: {}, usuario: Usuario});
    });
}

exports.usuario_update_post = function(req, res){
    usuario.findByIdAndUpdate(req.params.id, {"nombre": req.body.nombre}, function(err, Usuario){
        if(err){
            console.log(err);
            res.render('usuarios/update',  {errors: err.errors, usuario: new usuario({nombre: req.body.nombre, email: req.body.email})});
        }else{
            res.redirect('/usuarios');
        }
    });
}

exports.usuario_delete_post = function(req, res, next){
    usuario.findByIdAndDelete(req.body.id, function(err){
        if(err){
            next(err);
        }else{
            res.redirect('/usuarios');
        }
    });
}